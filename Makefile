SHELL := /usr/bin/env bash

setup: bundle-install build

setup-pull: bundle-install pull

bundle-install:
	bundle config --local set path '.bundle/gems'
	bundle install

start:
	bundle exec docker-sync-stack start

up:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml up

stop:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml down

down:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml down -v

sync:
	bundle exec docker-sync start --foreground

sync-stop:
	bundle exec docker-sync stop

sync-clean:
	bundle exec docker-sync-stack clean

pull:
	docker-compose pull

push:
	docker-compose push

build:
	docker-compose build

installdb:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml run --entrypoint /installdb.sh moodle

web:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml exec moodle bash
