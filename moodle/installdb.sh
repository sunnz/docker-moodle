#!/bin/bash

php /app/admin/cli/install_database.php --agree-license \
    --adminpass=admin \
    --adminemail=admin@localdomain.localdomain \
    --fullname=admin --shortname=admin

php /app/admin/tool/phpunit/cli/init.php
