docker-moodle
=============

Moodle development environment with docker-sync.

quickstart
----------

setting up (macos)::

    brew cask install docker
    brew install docker-compose
    brew install docker-completion
    sudo gem install bundler
    make setup

configure docker-sync::

    cp docker-sync-example.yml docker-sync.yml

Edit ``docker-sync.yml`` and update the path ``src: '/home/sunnz/dev/moodle_code/'`` to the top directory of your moodle
source code.

configure moodle::

    cp moodle-config.php path/to/moodle_code/config.php

run::

    make start # ctrl-c to stop

this starts everything.

install moodle database::

    make installdb

licence
-------

This project is licensed under the MIT licence

Copyright (c) 2020 sunnz <https://gitlab.com/sunnz>
